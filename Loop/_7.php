<?php
/**
 *  7. In ra tất cả số nguyên tố nhỏ hơn n (sử dụng vòng lặp for)
 *  Input :
 *         + Khai báo number
 *  Output:
 *         + Xác định $number có phải là số nguyên tố hay không ?
 *
 */

function isPrimeNumber($n) {
    // so nguyen n < 2 khong phai la so nguyen to
    if ($n < 2) {
        return false;
    }
    // check so nguyen to khi n >= 2
    $temp = sqrt ( $n );
    for($i = 2; $i <= $temp; $i ++) {
        if ($n % $i == 0) {
            return false;
        }
    }
    return true;
}
 
echo ("Các số nguyên tố nhỏ hơn 10 là: <br>");
for($i = 0; $i < 10; $i ++) {
    if (isPrimeNumber ( $i )) {
        echo ($i . " ");
    }
}