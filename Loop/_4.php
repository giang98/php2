<?php
/**
 *  4. Tính tổng số nguyên chẵn tử 1 -> n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */

$n = 10;
$sum = 0;

for($i = 0; $i <= $n; $i++){
    if($i % 2 == 0){
        $sum += $i;
    }
}

echo 'Tổng các số lẻ = '.$sum;