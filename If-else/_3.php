<?php
/**
 *  3. Bài toán tính tiền taxi với số km cho trước
 *  Input :
 *         + 1km đầu giá = 15000 đ
 *         + từ 1km đến 5km giá = 12000 đ
 *         + từ 6km trở đi giá  = 90000 đ
 *         + từ 140km trở đi được giảm 12 % tổng tiền
 *  Output:
 *         + Số tiền cần thanh toán
 */

 function price($km){
     if($km <= 1){
         $money = $km * 15000;
         echo 'Giá tiền : ' .$money; 
     }

     if( (1 < $km) && ($km <= 5) ){
        $money = $km * 12000;
        echo 'Giá tiền : ' .$money; 
    }

    if($km >= 6){
        $price = $km * 90000;

        if($km >= 140) {
            $money = $price/100*88;
            echo 'Giá tiền : ' .$money;
        }
        else echo 'Giá tiền : ' .$price; 
    }

 }

 price(0.5);
 price(5);
 price(9);
 price(150);